Henfield Storage is family-owned and operates as part of a successful business group which has been running for over 40 years. All of our personal and business customers benefit from low cost storage at our bases in North & West London, South & Southwest London, Central London, Surrey and Sussex.

Address: Millennium Business Centre, Humber Road, London NW2 6DW, UK

Phone: +44 2084 508602

